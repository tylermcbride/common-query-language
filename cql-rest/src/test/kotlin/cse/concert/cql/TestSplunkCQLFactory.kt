package cse.concert.cql

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import cse.concert.cql.factory.SplunkCQLFactory
import cse.concert.cql.splunk.operation.Operation
import cse.concert.cql.splunk.operation.OperatorType
import cse.concert.cql.splunk.operation.field.MultaryFieldOperation
import cse.concert.cql.splunk.operation.field.UnaryFieldOperation
import cse.concert.cql.splunk.operation.logical.LogicalOperation
import cse.concert.cql.splunk.operation.logical.MultaryLogicalOperation
import cse.concert.cql.splunk.operation.logical.UnaryLogicalOperation
import cse.concert.cql.splunk.specification.Specification
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import java.io.InputStream
import java.net.URI
import kotlin.test.Ignore
import kotlin.test.assertFailsWith

@ExtendWith(MockitoExtension::class)
class TestSplunkCQLFactory {

    private val specification: Specification = MAPPER.readValue(Thread.currentThread().contextClassLoader.getResourceAsStream("specification.json"), Specification::class.java)

    @Test
    fun testStringEqualsOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "String Field"
        operation.operator = OperatorType.EQUALS
        operation.operand = "covid"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(stringField = \"covid\")"))
    }

    @Test
    fun testNumberEqualsOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Number Field"
        operation.operator = OperatorType.EQUALS
        operation.operand = "19"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(numberField = 19)"))
    }

    @Test
    fun testDateEqualsOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Date Field"
        operation.operator = OperatorType.EQUALS
        operation.operand = "2020-01-01T00:00:00Z"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") = 1577836800)"))
    }

    @Test
    fun testStringStartsWithOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "String Field"
        operation.operator = OperatorType.STARTS_WITH
        operation.operand = "co"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(stringField = \"co*\")"))
    }

    @Test
    fun testNumberStartsWithOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "Number Field"
            operation.operator = OperatorType.STARTS_WITH
            operation.operand = "19"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: STARTS_WITH")
    }

    @Test
    fun testDateStartsWithOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "Date Field"
            operation.operator = OperatorType.STARTS_WITH
            operation.operand = "2020"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: STARTS_WITH")
    }

    @Test
    fun testStringEndsWithOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "String Field"
        operation.operator = OperatorType.ENDS_WITH
        operation.operand = "vid"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(stringField = \"*vid\")"))
    }

    @Test
    fun testNumberEndsWithOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "Number Field"
            operation.operator = OperatorType.ENDS_WITH
            operation.operand = "9"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: ENDS_WITH")
    }

    @Test
    fun testDateEndsWithOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "Date Field"
            operation.operator = OperatorType.ENDS_WITH
            operation.operand = "00:00:00"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: ENDS_WITH")
    }

    @Test
    fun testStringLessThanOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "String Field"
            operation.operator = OperatorType.LESS_THAN
            operation.operand = "covid"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: LESS_THAN")
    }

    @Test
    fun testNumberLessThanOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Number Field"
        operation.operator = OperatorType.LESS_THAN
        operation.operand = "19"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(numberField < 19)"))
    }

    @Test
    fun testDateLessThanOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Date Field"
        operation.operator = OperatorType.LESS_THAN
        operation.operand = "2020-01-01T00:00:00Z"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") < 1577836800)"))
    }

    @Test
    fun testStringLessThanOrEqualToOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "String Field"
            operation.operator = OperatorType.LESS_THAN_OR_EQUAL_TO
            operation.operand = "covid"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: LESS_THAN_OR_EQUAL_TO")
    }

    @Test
    fun testNumberLessThanOrEqualToOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Number Field"
        operation.operator = OperatorType.LESS_THAN_OR_EQUAL_TO
        operation.operand = "19"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(numberField <= 19)"))
    }

    @Test
    fun testDateLessThanOrEqualToOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Date Field"
        operation.operator = OperatorType.LESS_THAN_OR_EQUAL_TO
        operation.operand = "2020-01-01T00:00:00Z"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") <= 1577836800)"))
    }

    @Test
    fun testStringGreaterThanOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "String Field"
            operation.operator = OperatorType.GREATER_THAN
            operation.operand = "covid"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: GREATER_THAN")
    }

    @Test
    fun testNumberGreaterThanOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Number Field"
        operation.operator = OperatorType.GREATER_THAN
        operation.operand = "19"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(numberField > 19)"))
    }

    @Test
    fun testDateGreaterThanOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Date Field"
        operation.operator = OperatorType.GREATER_THAN
        operation.operand = "2020-01-01T00:00:00Z"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") > 1577836800)"))
    }

    @Test
    fun testStringGreaterThanOrEqualToOperator() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "String Field"
            operation.operator = OperatorType.GREATER_THAN_OR_EQUAL_TO
            operation.operand = "covid"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No matched field supports operand: GREATER_THAN_OR_EQUAL_TO")
    }

    @Test
    fun testNumberGreaterThanOrEqualToOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Number Field"
        operation.operator = OperatorType.GREATER_THAN_OR_EQUAL_TO
        operation.operand = "19"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(numberField >= 19)"))
    }

    @Test
    fun testDateGreaterThanOrEqualToOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "Date Field"
        operation.operator = OperatorType.GREATER_THAN_OR_EQUAL_TO
        operation.operand = "2020-01-01T00:00:00Z"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") >= 1577836800)"))
    }

    @Test
    fun testStringInOperator() {
        val operation = MultaryFieldOperation()
        operation.field = "String Field"
        operation.operator = OperatorType.IN
        operation.operands = listOf("covid", "19")
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(stringField IN (\"covid\",\"19\"))"))
    }

    @Test
    fun testNumberInOperator() {
        val operation = MultaryFieldOperation()
        operation.field = "Number Field"
        operation.operator = OperatorType.IN
        operation.operands = listOf("420", "19")
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(numberField IN (420,19))"))
    }

    @Test
    fun testDateInOperator() {
        val operation = MultaryFieldOperation()
        operation.field = "Date Field"
        operation.operator = OperatorType.IN
        operation.operands = listOf("2020-01-01T00:00:00Z", "2020-02-01T00:00:00Z")
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") IN (1577836800,1580515200))"))
    }

    @Test
    fun testFieldDoesNotExist() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "Unknown Field"
            operation.operator = OperatorType.EQUALS
            operation.operand = "covid"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("No queryable fields match: Unknown Field")
    }

    @Test
    fun testInvalidNumber() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "Number Field"
            operation.operator = OperatorType.EQUALS
            operation.operand = "NaN"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("Operand(s) are invalid.")
    }

    @Test
    fun testInvalidDate() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            val operation = UnaryFieldOperation()
            operation.field = "Date Field"
            operation.operator = OperatorType.EQUALS
            operation.operand = "Easter Monday"
            factory.buildQuery(specification, operation)
        }
        assertThat(exception.message).isEqualTo("Operand(s) are invalid.")
    }

    @Test
    fun testWildCard() {
        val operation = UnaryFieldOperation()
        operation.field = "*"
        operation.operator = OperatorType.EQUALS
        operation.operand = "covid"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(stringField = \"covid\")"))
    }

    @Test
    fun testStringWildCard() {
        val operation = UnaryFieldOperation()
        operation.field = "*Field"
        operation.operator = OperatorType.EQUALS
        operation.operand = "covid"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(stringField = \"covid\")"))
    }

    @Test
    fun testNumberWildCard() {
        val operation = UnaryFieldOperation()
        operation.field = "*Field"
        operation.operator = OperatorType.EQUALS
        operation.operand = "19"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(numberField = 19 EQUALS stringField = \"19\")"))
    }

    @Test
    fun testDateWildCard() {
        val operation = UnaryFieldOperation()
        operation.field = "*Field"
        operation.operator = OperatorType.EQUALS
        operation.operand = "2020-01-01T00:00:00Z"
        assertThat(factory.buildQuery(specification, operation))
                .isEqualTo(buildExpectedQuery("(strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") = 1577836800 EQUALS stringField = \"2020-01-01T00:00:00Z\")"))
    }

    @Test
    fun testNotOperator() {
        val operation = UnaryFieldOperation()
        operation.field = "String Field"
        operation.operator = OperatorType.EQUALS
        operation.operand = "covid"

        val notOperation = UnaryLogicalOperation()
        notOperation.operator = OperatorType.NOT
        notOperation.operand = operation

        assertThat(factory.buildQuery(specification, notOperation))
                .isEqualTo(buildExpectedQuery("NOT (stringField = \"covid\")"))
    }

    @Test
    fun testAndOperator() {
        val stringOperation = UnaryFieldOperation()
        stringOperation.field = "String Field"
        stringOperation.operator = OperatorType.EQUALS
        stringOperation.operand = "covid"

        val numberOperation = UnaryFieldOperation()
        numberOperation.field = "Number Field"
        numberOperation.operator = OperatorType.EQUALS
        numberOperation.operand = "19"

        val notOperation = MultaryLogicalOperation()
        notOperation.operator = OperatorType.AND
        notOperation.operands = listOf<Operation>(stringOperation, numberOperation)

        assertThat(factory.buildQuery(specification, notOperation))
                .isEqualTo(buildExpectedQuery("((stringField = \"covid\") AND (numberField = 19))"))
    }

    @Test
    fun testOrOperator() {
        val stringOperation = UnaryFieldOperation()
        stringOperation.field = "String Field"
        stringOperation.operator = OperatorType.EQUALS
        stringOperation.operand = "covid"

        val numberOperation = UnaryFieldOperation()
        numberOperation.field = "Number Field"
        numberOperation.operator = OperatorType.EQUALS
        numberOperation.operand = "19"

        val notOperation = MultaryLogicalOperation()
        notOperation.operator = OperatorType.OR
        notOperation.operands = listOf<Operation>(stringOperation, numberOperation)

        assertThat(factory.buildQuery(specification, notOperation))
                .isEqualTo(buildExpectedQuery("((stringField = \"covid\") OR (numberField = 19))"))
    }

    @Test
    fun testComplexOperator() {
        val resource = Thread.currentThread().contextClassLoader.getResourceAsStream("complex_operation.json")
        assertThat(factory.buildQuery(specification, MAPPER.readValue(resource, Operation::class.java)))
                .isEqualTo(buildExpectedQuery("(NOT (numberField = 19 OR stringField = \"19\") AND (strptime('dateField', \"%Y-%m-%dT%H:%M:%S\") > 1577836800) AND ((numberField IN (19,420)) OR (numberField > 1337)))"))
    }

    private fun buildExpectedQuery(whereClause: String): String {
        return "search source=testSpecification | where $whereClause | rename \"dateField\" AS \"Date Field\", \"numberField\" AS \"Number Field\", \"stringField\" AS \"String Field\", \"unqueryable\" AS \"Unqueryable\" | table \"Date Field\", \"Number Field\", \"String Field\", \"Unqueryable\""
    }

    companion object {
        private val MAPPER = ObjectMapper().registerModule(KotlinModule())
        private val factory = SplunkCQLFactory()
    }
}