package cse.concert.cql

import kotlin.jvm.JvmStatic
import org.springframework.boot. SpringApplication

object Application {
    @JvmStatic
    fun main(args: Array<String>) {
        SpringApplication.run(Application::class.java, *args)
    }
}