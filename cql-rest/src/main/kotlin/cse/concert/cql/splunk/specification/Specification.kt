package cse.concert.cql.splunk.specification

class Specification {
    lateinit var name: String
    lateinit var description: String
    lateinit var fields: Map<String, Field>
}