package cse.concert.cql.splunk.operation

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME
import cse.concert.cql.splunk.operation.field.MultaryFieldOperation
import cse.concert.cql.splunk.operation.field.UnaryFieldOperation
import cse.concert.cql.splunk.operation.logical.MultaryLogicalOperation
import cse.concert.cql.splunk.operation.logical.UnaryLogicalOperation
import io.swagger.annotations.ApiModelProperty

@JsonTypeInfo(use = NAME, property = "operator", visible = true)
@JsonSubTypes(
        JsonSubTypes.Type(value = UnaryLogicalOperation::class, name = "not"),
        JsonSubTypes.Type(value = MultaryLogicalOperation::class, name = "and"),
        JsonSubTypes.Type(value = MultaryLogicalOperation::class, name = "or"),
        JsonSubTypes.Type(value = UnaryFieldOperation::class, name = "equals"),
        JsonSubTypes.Type(value = UnaryFieldOperation::class, name = "startsWith"),
        JsonSubTypes.Type(value = UnaryFieldOperation::class, name = "endsWith"),
        JsonSubTypes.Type(value = UnaryFieldOperation::class, name = "lessThan"),
        JsonSubTypes.Type(value = UnaryFieldOperation::class, name = "lessThanOrEqualTo"),
        JsonSubTypes.Type(value = UnaryFieldOperation::class, name = "greaterThan"),
        JsonSubTypes.Type(value = UnaryFieldOperation::class, name = "greaterThanOrEqualTo"),
        JsonSubTypes.Type(value = MultaryFieldOperation::class, name = "in")
)
abstract class Operation {
        @ApiModelProperty(value = "operator", example = "and")
        lateinit var operator: OperatorType
}