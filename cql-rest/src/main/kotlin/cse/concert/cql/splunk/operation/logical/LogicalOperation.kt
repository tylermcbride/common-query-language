package cse.concert.cql.splunk.operation.logical

import cse.concert.cql.splunk.operation.Operation

abstract class LogicalOperation : Operation()