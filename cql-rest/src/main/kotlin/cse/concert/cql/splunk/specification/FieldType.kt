package cse.concert.cql.splunk.specification

import com.fasterxml.jackson.annotation.JsonProperty

enum class FieldType {
    @JsonProperty("string")
    STRING,
    @JsonProperty("number")
    NUMBER,
    @JsonProperty("datetime")
    DATETIME
}