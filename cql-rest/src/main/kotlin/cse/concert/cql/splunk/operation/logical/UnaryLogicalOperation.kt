package cse.concert.cql.splunk.operation.logical

import cse.concert.cql.splunk.operation.Operation

class UnaryLogicalOperation : LogicalOperation() {
    lateinit var operand: Operation
}