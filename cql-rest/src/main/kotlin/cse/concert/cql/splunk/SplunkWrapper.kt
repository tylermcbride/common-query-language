package cse.concert.cql.splunk

data class SplunkWrapper<T> (
    val results: MutableList<T>
)