package cse.concert.cql.splunk.specification

import kotlin.properties.Delegates

class Field {
    lateinit var mappedField: String
    lateinit var type: FieldType
    var queryable by Delegates.notNull<Boolean>()
    var format: String? = null
}
