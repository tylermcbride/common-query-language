package cse.concert.cql.splunk.operation

import com.fasterxml.jackson.annotation.JsonProperty

enum class OperatorType {
    @JsonProperty("and")
    AND,
    @JsonProperty("or")
    OR,
    @JsonProperty("not")
    NOT,
    @JsonProperty("equals")
    EQUALS,
    @JsonProperty("startsWith")
    STARTS_WITH,
    @JsonProperty("endsWith")
    ENDS_WITH,
    @JsonProperty("lessThan")
    LESS_THAN,
    @JsonProperty("lessThanOrEqualTo")
    LESS_THAN_OR_EQUAL_TO,
    @JsonProperty("greaterThan")
    GREATER_THAN,
    @JsonProperty("greaterThanOrEqualTo")
    GREATER_THAN_OR_EQUAL_TO,
    @JsonProperty("in")
    IN
}