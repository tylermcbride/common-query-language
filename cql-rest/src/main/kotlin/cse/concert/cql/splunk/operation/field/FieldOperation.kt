package cse.concert.cql.splunk.operation.field

import cse.concert.cql.splunk.operation.Operation

abstract class FieldOperation : Operation() {
    lateinit var field: String
}