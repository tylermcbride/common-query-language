package cse.concert.cql.splunk

import kotlin.jvm.JvmStatic
import org.springframework.boot.SpringApplication

@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
@lombok.Data
class SplunkObject constructor() {
    private var host: kotlin.String? = null
    private var linecount: kotlin.String? = null
    private var source: kotlin.String? = null
    private var _indextime: java.time.Instant? = null
}