package cse.concert.cql.controllers

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.beans.factory.annotation.Autowired
import cse.concert.cql.service.SplunkService
import cse.concert.cql.service.SpecificationService
import cse.concert.cql.controllers.PageableParams
import cse.concert.cql.splunk.operation.Operation
import cse.concert.cql.splunk.specification.Specification
import org.springframework.web.bind.annotation.GetMapping
import kotlin.jvm.Throws
import java.io.IOException
import org.springframework.web.bind.annotation.RequestParam
import springfox.documentation.annotations.ApiIgnore
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody

@RestController
@RequestMapping("/search")
class SearchController constructor() {
    @Autowired
    private var splunkService: SplunkService? = null
    @Autowired
    private var specificationService: SpecificationService? = null

    @PageableParams
    @GetMapping("/splunk")
    @Throws(IOException::class)
    fun splunk(@RequestParam(value = "query", required = false) query: String, @ApiIgnore pageable: org.springframework.data.domain.Pageable?): MutableList<Any> {
        return splunkService!!.search("search " + query, pageable, Any::class.java)
    }

    @PatchMapping("/{type}")
    @Throws(IOException::class)
    fun commonQueryLanguage(@PathVariable(name = "type") type: String, @RequestBody operation: Operation): MutableList<Any> {
        val specification: Specification = specificationService!!.getSpecification(type)
        return splunkService!!.search(specification, operation, Any::class.java)
    }
}