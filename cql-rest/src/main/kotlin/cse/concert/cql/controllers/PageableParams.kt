package cse.concert.cql.controllers

import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * Swagger has a known issue where it will ask for the wrong parameters to build a Pageable, causing a default Pageable
 * to be generated (https://github.com/springfox/springfox/issues/2623). This annotation manually sets the parameters
 * that Swagger will ask for to build a Pageable. Should be combined with an @ApiIgnore annotation on the Pageable
 * itself
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Retention(RetentionPolicy.RUNTIME)
@ApiImplicitParams(ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Results page you want to retrieve (0..N)"), ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Number of rows per page"), ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property,(ASC|DESC) "
        + "Default sort order is ascending. " + "Multiple sort criteria are supported."))
annotation class PageableParams