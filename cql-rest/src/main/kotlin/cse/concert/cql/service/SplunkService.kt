package cse.concert.cql.service

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.ObjectMapper
import com.splunk.*
import cse.concert.cql.factory.CQLFactory
import cse.concert.cql.factory.SplunkCQLFactory
import cse.concert.cql.splunk.SplunkWrapper
import cse.concert.cql.splunk.operation.Operation
import cse.concert.cql.splunk.specification.Specification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.InputStream
import java.lang.IllegalArgumentException
import javax.annotation.PostConstruct

@Component
class SplunkService {
    companion object {
        private const val OUTPUT_MODE = "json"
        private val MAPPER = ObjectMapper()
        private val FACTORY: CQLFactory = SplunkCQLFactory()

        /**
         * This method will apply the respective sort pipe to the query.
         *
         * @param query The query.
         * @param sort  The [Sort].
         * @return A query with the respective sort pipe applied.
         */
        @JvmStatic
        fun applySort(query: String, sort: Sort): String {
            if (sort.isEmpty) {
                return query
            }
            val sorting = sort.asSequence()
                    .map { getOperator(it) + it.property }
                    .joinToString(" ")
            return "$query | sort $sorting"
        }

        /**
         * Will retrieve the respective splunk operator.
         *
         * @param order The [Sort.Order].
         * @return The respective splunk operator.
         */
        private fun getOperator(order: Sort.Order): String {
            return when (order.direction) {
                Sort.Direction.ASC -> "+"
                Sort.Direction.DESC -> "-"
                else -> throw IllegalArgumentException("Unsupported direction: ${order.direction}")
            }
        }

        private fun <T> convertToPage(results: MutableList<T>, pageable: Pageable): Page<T> {
            return PageImpl(results, pageable, results.size.toLong())
        }

        init {
            MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        }
    }

    private var service: Service? = null

    @PostConstruct
    fun postConstruct() {
        HttpService.setSslSecurityProtocol(SSLSecurityProtocol.TLSv1_2)
        // Create a map of arguments and add login parameters
        val loginArgs = ServiceArgs()
        loginArgs["scheme"] = "https"
        loginArgs.setUsername("")
        loginArgs.setPassword("")
        loginArgs.setHost("")
        loginArgs.setPort(8089)

        // Create a Service instance and log in with the argument map
        service = Service.connect(loginArgs)
    }

    @Throws(IOException::class)
    fun <T> search(specification: Specification, operation: Operation, pageable: Pageable, type: Class<T>): Page<T> {
        return convertToPage(search(FACTORY.buildQuery(specification, operation), pageable, type), pageable)
    }

    @Throws(IOException::class)
    fun <T> search(specification: Specification, operation: Operation, type: Class<T>): MutableList<T> {
        return search(FACTORY.buildQuery(specification, operation), null, type)
    }

    @Throws(IOException::class)
    fun <T> search(query: String, pageable: Pageable?, type: Class<T>): MutableList<T> {
        return getWrapper(oneshotSearch(query, pageable), type).results
    }

    private fun oneshotSearch(query: String, pageable: Pageable?): InputStream {
        var query = query
        val args = CollectionArgs()
        args["output_mode"] = OUTPUT_MODE
        if (pageable != null) {
            args.setOffset(Math.toIntExact(pageable.offset))
            args.setCount(pageable.pageSize)
            query = applySort(query, pageable.sort)
        }
        return service!!.oneshotSearch(query, args)
    }

    @Throws(IOException::class)
    private fun <T> getWrapper(inputStream: InputStream, type: Class<T>): SplunkWrapper<T> {
        return MAPPER.readValue(inputStream, getWrapperType(type))
    }

    private fun <T> getWrapperType(type: Class<T>?): JavaType {
        return MAPPER.typeFactory.constructParametricType(SplunkWrapper::class.java, type)
    }
}