package cse.concert.cql.service

import com.fasterxml.jackson.databind.ObjectMapper
import cse.concert.cql.splunk.specification.Specification
import org.springframework.stereotype.Component
import java.io.IOException
import java.util.*
import javax.annotation.PostConstruct

@Component
class SpecificationService {
    private val specifications: MutableList<Specification> = ArrayList()

    @PostConstruct
    @Throws(IOException::class)
    fun postConstruct() {
        specifications.add(MAPPER.readValue(Thread.currentThread().contextClassLoader.getResourceAsStream("specification.json"), Specification::class.java))
        specifications.add(MAPPER.readValue(Thread.currentThread().contextClassLoader.getResourceAsStream("corona.json"), Specification::class.java))
    }

    fun getSpecification(type: String?): Specification {
        return Optional.ofNullable(type)
                .flatMap { t: String ->
                    specifications.stream()
                            .filter { specification: Specification -> t === specification.name }
                            .findFirst()
                }
                .orElseThrow { IllegalArgumentException(String.format(UNSUPPORTED_TYPE_MSG, type)) }
    }

    companion object {
        private const val UNSUPPORTED_TYPE_MSG = "Unsupported type: %s"
        private val MAPPER = ObjectMapper()
    }
}