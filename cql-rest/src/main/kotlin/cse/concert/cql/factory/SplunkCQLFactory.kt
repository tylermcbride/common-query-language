package cse.concert.cql.factory

import cse.concert.cql.service.SplunkService.Companion.applySort
import cse.concert.cql.splunk.operation.Operation
import cse.concert.cql.splunk.operation.OperatorType
import cse.concert.cql.splunk.operation.field.FieldOperation
import cse.concert.cql.splunk.operation.field.MultaryFieldOperation
import cse.concert.cql.splunk.operation.field.UnaryFieldOperation
import cse.concert.cql.splunk.operation.logical.LogicalOperation
import cse.concert.cql.splunk.operation.logical.MultaryLogicalOperation
import cse.concert.cql.splunk.operation.logical.UnaryLogicalOperation
import cse.concert.cql.splunk.specification.Field
import cse.concert.cql.splunk.specification.FieldType
import cse.concert.cql.splunk.specification.Specification
import org.springframework.data.domain.Pageable
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import kotlin.streams.toList

class SplunkCQLFactory : CQLFactory {
    override fun buildQuery(specification: Specification, operation: Operation): String {
        val pipes = mutableListOf<String>()
        pipes.add("search source=${specification.name}")
        pipes.add("where " + buildOperation(operation, specification))
        val aliasedFields = getAliasedFields(specification)
        if (aliasedFields.isNotEmpty()) {
            pipes.add("rename " + aliasedFields.asSequence()
                    .map { "\"${it.value.mappedField}\" AS \"${it.key}\"" }
                    .joinToString(", "))
        }
        pipes.add("table ${buildTable(specification)}")
        return pipes.joinToString(" | ")
    }

    override fun buildQuery(specification: Specification, operation: Operation, pageable: Pageable): String {
        return applySort(buildQuery(specification, operation), pageable.sort)
    }

    private fun buildTable(specification: Specification): String {
        return specification.fields.keys.joinToString(", ") { "\"$it\"" }
    }

    private fun getAliasedFields(specification: Specification): Map<String, Field> {
        return specification.fields.entries.asSequence()
                .filter { it.key != it.value.mappedField }
                .associateBy({ it.key }, { it.value })
    }

    private fun buildOperation(operation: Operation, specification: Specification): String {
        return when (operation) {
            is LogicalOperation -> buildLogicalOperation(operation, specification)
            is FieldOperation -> buildFieldOperations(operation, getSupportedMatchingFields(operation, specification))
            else -> throw IllegalArgumentException(String.format(UNSUPPORTED_OPERATION_MSG, operation.javaClass))
        }
    }

    private fun buildLogicalOperation(operation: LogicalOperation, specification: Specification): String {
        return when (operation) {
            is UnaryLogicalOperation -> buildUnaryLogicalOperation(operation, specification)
            is MultaryLogicalOperation -> buildMultaryLogicalOperation(operation, specification)
            else -> throw IllegalArgumentException(String.format(UNSUPPORTED_OPERATION_MSG, operation.javaClass))
        }
    }

    private fun buildUnaryLogicalOperation(operation: UnaryLogicalOperation, specification: Specification): String {
        return "${operation.operator} ${buildOperation(operation.operand, specification)}"
    }

    private fun buildMultaryLogicalOperation(operation: MultaryLogicalOperation, specification: Specification): String {
        val operands = operation.operands.asSequence()
                .map { buildOperation(it, specification) }
                .joinToString(" ${operation.operator} ")
        return "($operands)"
    }

    private fun buildFieldOperations(operation: FieldOperation, fields: List<Field>): String {
        val operations = fields.asSequence()
                .mapNotNull {
                    try {
                        buildFieldOperation(it, operation)
                    } catch (e: Exception) {
                        when (e) {
                            is NumberFormatException,
                            is DateTimeParseException -> null
                            else -> throw e
                        }
                    }
                }.toList()
        if (operations.isEmpty()) {
            throw IllegalArgumentException(INVALID_OPERANDS_MSG)
        }
        return "(${operations.joinToString(" OR ")})"
    }

    private fun buildFieldOperation(field: Field, operation: Operation): String {
        return when (operation) {
            is UnaryFieldOperation -> buildUnaryFieldOperation(field, operation)
            is MultaryFieldOperation -> buildMultarFieldOperation(field, operation)
            else -> throw IllegalArgumentException(String.format(UNSUPPORTED_OPERATION_MSG, operation.javaClass))
        }
    }

    private fun buildUnaryFieldOperation(field: Field, operation: UnaryFieldOperation): String {
        val splunkField = getSplunkField(field)
        val splunkOperator = getSplunkOperator(operation.operator)
        val operand = convertToSplunk(field, operation.operator, operation.operand)
        return "$splunkField $splunkOperator $operand"
    }

    private fun buildMultarFieldOperation(field: Field, operation: MultaryFieldOperation): String {
        val splunkField = getSplunkField(field)
        val splunkOperator = getSplunkOperator(operation.operator)
        val operands = operation.operands.asSequence()
                .map { convertToSplunk(field, operation.operator, it) }
                .joinToString(",")
        return "$splunkField $splunkOperator ($operands)"
    }

    private fun getSplunkField(field: Field): String {
        return when (field.type) {
            FieldType.STRING,
            FieldType.NUMBER -> field.mappedField
            FieldType.DATETIME -> {
                val format = field.format ?: throw IllegalArgumentException(MISSING_FORMAT_MSG)
                return "strptime('${field.mappedField}', \"$format\")"
            }
        }
    }

    /**
     * Returns whether the [OperatorType] is supported given the [Field.type].
     *
     * @param operator The [OperatorType].
     * @param field The [Field].
     * @return The support.
     */
    private fun isSupported(field: Field, operator: OperatorType): Boolean {
        return when (field.type) {
            FieldType.STRING -> when (operator) {
                OperatorType.EQUALS,
                OperatorType.STARTS_WITH,
                OperatorType.ENDS_WITH,
                OperatorType.IN -> true
                else -> false
            }
            FieldType.NUMBER -> when (operator) {
                OperatorType.EQUALS,
                OperatorType.LESS_THAN,
                OperatorType.LESS_THAN_OR_EQUAL_TO,
                OperatorType.GREATER_THAN,
                OperatorType.GREATER_THAN_OR_EQUAL_TO,
                OperatorType.IN -> true
                else -> false
            }
            FieldType.DATETIME -> when (operator) {
                OperatorType.EQUALS,
                OperatorType.LESS_THAN,
                OperatorType.LESS_THAN_OR_EQUAL_TO,
                OperatorType.GREATER_THAN,
                OperatorType.GREATER_THAN_OR_EQUAL_TO,
                OperatorType.IN -> true
                else -> false
            }
        }
    }

    /**
     * Returns the Splunk representation of the [OperatorType].
     *
     * @param operator The [OperatorType].
     * @return The Splunk representation.
     */
    private fun getSplunkOperator(operator: OperatorType?): String {
        return when (operator) {
            OperatorType.NOT -> "NOT"
            OperatorType.AND -> "AND"
            OperatorType.OR -> "OR"
            OperatorType.EQUALS,
            OperatorType.STARTS_WITH,
            OperatorType.ENDS_WITH -> "="
            OperatorType.LESS_THAN -> "<"
            OperatorType.LESS_THAN_OR_EQUAL_TO -> "<="
            OperatorType.GREATER_THAN -> ">"
            OperatorType.GREATER_THAN_OR_EQUAL_TO -> ">="
            OperatorType.IN -> "IN"
            else -> throw IllegalArgumentException(String.format(UNSUPPORTED_OPERATOR_MSG, operator))
        }
    }

    /**
     * Will convert an operand to the Splunk representation.
     *
     * @param field    The [Field].
     * @param operator The [OperatorType].
     * @param operand  The operand.
     * @return The operand in Splunk representation.
     */
    private fun convertToSplunk(field: Field, operator: OperatorType, operand: String): String {
        return when (field.type) {
            FieldType.STRING -> convertToSplunkString(operator, operand)
            FieldType.NUMBER -> convertToSplunkNumber(operand)
            FieldType.DATETIME -> convertToSplunkDateTime(operand)
        }
    }

    private fun convertToSplunkString(operator: OperatorType, operand: String): String {
        var operand = operand.replace("\"", "\\\"")
        if (OperatorType.STARTS_WITH == operator) {
            operand = "$operand*"
        } else if (OperatorType.ENDS_WITH == operator) {
            operand = "*$operand"
        }
        return "\"$operand\""
    }

    private fun convertToSplunkNumber(operand: String): String {
        return operand.toLong().toString()
    }

    private fun convertToSplunkDateTime(operand: String): String {
        val dateTime = OffsetDateTime.parse(operand, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        return (dateTime.toInstant().toEpochMilli() / 1000).toString()
    }

    /**
     * Returns a list of supported [Field]s from the provided [Specification] that correspond to the given
     * field. The field will accept '*' as a wildcard.
     *
     * @param operation     The [Operation].
     * @param specification The [Specification].
     * @return The corresponding list of [Field]s.
     */
    private fun getSupportedMatchingFields(operation: FieldOperation, specification: Specification): List<Field> {
        val wildcardRegex = operation.field.replace("*", ".*").toRegex()
        val matchingFields = specification.fields.entries.stream()
                .filter { entry -> entry.value.queryable && entry.key.matches(wildcardRegex) }
                .map(Map.Entry<String, Field>::value)
                .toList()
        require(matchingFields.isNotEmpty()) { java.lang.String.format(UNMATCHED_FIELDS_MSG, operation.field) }
        return getSupportedFields(operation, matchingFields)
    }

    /**
     * Returns a reduced list of supported [Field]s from the provided list of [Field]s based on the
     * [Operation.operator].
     *
     * @param operation    The [Operation].
     * @param fields       The list [Field]s.
     * @return The reduced list of [Field]s.
     */
    private fun getSupportedFields(operation: FieldOperation, fields: List<Field>): List<Field> {
        val supportedFields = fields.asSequence()
                .filter { isSupported(it, operation.operator) }
                .toList()
        if (supportedFields.isEmpty()) {
            throw IllegalArgumentException(String.format(MATCHED_FIELDS_UNSUPPORTED_MSG, operation.operator))
        }
        return supportedFields
    }

    companion object {
        private const val UNSUPPORTED_OPERATION_MSG = "Unsupported operation: %s"
        private const val UNSUPPORTED_OPERATOR_MSG = "Unsupported operator: %s"
        private const val UNMATCHED_FIELDS_MSG = "No queryable fields match: %s"
        private const val MATCHED_FIELDS_UNSUPPORTED_MSG = "No matched field supports operand: %s"
        private const val INVALID_OPERANDS_MSG = "Operand(s) are invalid."
        private const val MISSING_FORMAT_MSG = "Field %s is missing date format"
    }
}