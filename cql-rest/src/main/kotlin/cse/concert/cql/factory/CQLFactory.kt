package cse.concert.cql.factory

import cse.concert.cql.splunk.operation.Operation
import cse.concert.cql.splunk.specification.Specification
import org.springframework.data.domain.Pageable
import java.io.IOException

interface CQLFactory {
    @Throws(IOException::class)
    fun buildQuery(specification: Specification, operation: Operation): String

    @Throws(IOException::class)
    fun buildQuery(specification: Specification, operation: Operation, pageable: Pageable): String?
}